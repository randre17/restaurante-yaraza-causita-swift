//
//  RestaurantesViewController.swift
//  Restaurante Yaraza Kausita
//
//  Created by Rafael on 10/16/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit


class RestaurantesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tabla: UITableView!
    
    var listado = [Restaurante]()
    typealias ObjectJ = [String: Any]
    var nombrePlatillo : String = ""
    
    
    var nomPlat: String = ""
    var rutaImgPlat: String = ""
    var prePlat: String = ""
    var imgPlat: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let url = "http://192.168.58.1:8080/yaraza-causita-api-backend/v1/rest/get/listadoPlatillos"
        // Do any additional setup after loading the view.
        if let urlRequest = URL(string: url){
            let session = URLSession.shared.dataTask(with: urlRequest) { (datos, cabezera, error) in
                if let err = error{
                    print("Se notifico un error en la session \(err)")
                }else{
                    
                    if let dts = datos {
                        print("Existe informacion \(dts)")
                        if let jason = try? JSONSerialization.jsonObject(with: dts, options: []){
                            
                            if let jasonData = jason as? ObjectJ{
                                let data = jasonData["listaPlatillos"] as? [ObjectJ] ?? []
                                
                                for datos in data{
                                    let nomPlatillo = datos["nomPlatillo"] as? String ?? ""
                                    let imagPlatillo = datos["imagenPlatillo"] as? String ?? ""
                                    let preciPlatillo = datos["precioPlatillo"] as? String ?? ""
                                    
                                    
                                    let objeto = Restaurante(fotoPla: imagPlatillo, nomPla: nomPlatillo, prePla: preciPlatillo)
                                    self.listado.append(objeto)
                                    print(nomPlatillo)
                                    print(imagPlatillo)
                                    
                                    
                                    
                                }
                                DispatchQueue.main.async {
                                    self.tabla.reloadData()
                                    
                                }
                            }
                            
                        }else{
                            print("Error la inforacion no es un JSON")
                        }
                    }
                }
            }
            session.resume()
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vista = segue.destination as? AddItemCarritoViewController{
            vista.nomPlatillo = nomPlat
            vista.rutaImagen = rutaImgPlat
            vista.precio = prePlat
        }
    }
    
    @IBAction func irResumenCarrito(_ sender: Any) {
        print("Selecciono Carrito Resumen")
        performSegue(withIdentifier: "goResumenCarrito", sender: nil)
    }
    @IBAction func btnResumen(_ sender: Any) {
        //print("Selecciono Carrito Resumen")
        //performSegue(withIdentifier: "goResumenCarrito", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listado.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "cellYK", for: indexPath)
        
        let lblImag = celda.viewWithTag(1) as? UIImageView
        let lblNom = celda.viewWithTag(2) as? UILabel
        let lblPre = celda.viewWithTag(3) as? UILabel
        
        let variables = self.listado[indexPath.row]
        let urlImagen = URL(string: variables.fotoPla)!
        
        if let data = try? Data(contentsOf: urlImagen) {
                   lblImag?.image = UIImage(data: data)
               }
        
        //lblImag?.loadPhoto(url: variables.fotoPla)
        
        
        
        lblNom?.text = variables.nomPla
        lblPre?.text = variables.prePla
        return celda
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let platilloSel = listado[indexPath.row]
        print("Selecciono" + String(platilloSel.nomPla ))
        
        nomPlat = platilloSel.nomPla
        rutaImgPlat = platilloSel.fotoPla
        prePlat = platilloSel.prePla
        //imgPlat = lblImag
            
        performSegue(withIdentifier: "goDetallePlatillo", sender: nil)
    }
    
    
}

extension UIImageView{
    func loadPhoto(url: String) {
        if let objURL = URL(string: url){
            
            DispatchQueue.global().async {
                
                
                
                if let datos = try? Data(contentsOf: objURL){
                    if let imagen = UIImage(data: datos){
                        DispatchQueue.main.async {
                            self.image = imagen
                        }
                    }else {
                    }     //nose pudo convertir a img
                }else{
                    //ocurrio
                }
            }
        }
    }
}


