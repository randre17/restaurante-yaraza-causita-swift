//
//  CoreDataManager.swift
//  Restaurante Yaraza Kausita
//
//  Created by Henry Daniel on 12/10/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager{
    private let contenier : NSPersistentContainer!
    
    init(){
        contenier = NSPersistentContainer(name: "CarritoBD")
        contenier.loadPersistentStores { (stDes, err) in
            if let err = err {
                print("Ha ocurrido un error en la BD \(stDes) - \(err)")
            } else {
                print("Se ha cargado correctamente la BD")
            }
        }
    }
    
    func agregar(nombre: String, pu:String, cant: String, pt:String){
        let context = contenier.viewContext
        let item = Carrito(context: context)
        item.nombrePlatillo = nombre
        item.precioUnitario = pu
        item.cantidad = cant
        item.precioTotal = pt
        
        do{
            try context.save()
            print("Se grabo correctamente el producto")
        } catch {
            print("No se pudo grabar el producto \(error)")
        }
    }
    
    func obtener() -> [Carrito] {
        let context = contenier.viewContext
        let query : NSFetchRequest<Carrito> = Carrito.fetchRequest()
        do {
            let listaItemsCarrito = try context.fetch(query)
            return listaItemsCarrito
            print("Se cargo correctamente los items del carrito")
        } catch {
            print("Error al listar carrito \(error)")
        }
        return []
    }
    
}
