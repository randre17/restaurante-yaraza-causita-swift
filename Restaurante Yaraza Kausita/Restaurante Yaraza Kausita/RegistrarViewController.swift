//
//  RegistrarViewController.swift
//  Restaurante Yaraza Kausita
//
//  Created by Henry Daniel on 11/26/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import FirebaseAuth

class RegistrarViewController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtContraseña: UITextField!
    @IBOutlet weak var btnRegistrar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

 
    }
    

 
    @IBAction func Registrar(_ sender: Any) {
        
        if let email = txtEmail.text, let password = txtContraseña.text{
            Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                if let result = result, error == nil {
                    
                    let alertController = UIAlertController(title: "Good", message: "Se ha registrado el usuario correctamente",
                                                           preferredStyle: .alert)
                    
                     alertController.addAction(UIAlertAction.init(title: "Aceptar", style: .default))
                    
                    self.present(alertController, animated: true, completion: nil)
                     
                    
                    
                }else {
                    let alertController = UIAlertController(title: "Error", message:
                        "Se ha producido un error registrando el usuario", preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction.init(title: "Aceptar", style: .default))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}
