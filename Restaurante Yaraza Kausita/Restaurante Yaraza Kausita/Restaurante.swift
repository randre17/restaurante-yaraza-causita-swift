//
//  RestauranteC.swift
//  Restaurante Yaraza Kausita
//
//  Created by Henry Daniel on 12/10/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import Foundation

class Restaurante{
    let fotoPla : String
    let nomPla :  String
    let prePla :  String
    
    init(fotoPla : String, nomPla : String, prePla : String){
        self.fotoPla = fotoPla
        self.nomPla = nomPla
        self.prePla = prePla
        
    }
}
