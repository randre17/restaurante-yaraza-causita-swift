//
//  AddItemCarritoViewController.swift
//  Restaurante Yaraza Kausita
//
//  Created by Rafael Ramirez on 12/11/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit

class AddItemCarritoViewController: UIViewController {

    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var ivImagen: UIImageView!
    @IBOutlet weak var lblPrecio: UILabel!
    @IBOutlet weak var txtCantidad: UITextField!
    @IBOutlet weak var lblTotal: UILabel!
    
    var listaCarrito = [ItemCarrito]()
    //var item = ItemCarrito(nombre: "", precioUnitario: "", cantidad: "", total: "")
    
    var nomPlatillo: String = ""
    var rutaImagen: String = ""
    var precio: String = ""
    //var cantidad: String = ""
    //var total: String = ""

    private let managerDBCarrito = CoreDataManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitulo.text = String (nomPlatillo)
        //traerImagen()
        lblPrecio.text = String (precio)
        //cargarFoto()
        // Do any additional setup after loading the view.
    }
    
    /*func traerImagen(){
        let urlImagen = URL(string: rutaImagen)!
        
        if let data = try? Data(contentsOf: urlImagen) {
                   ivImagen?.image = UIImage(data: data)
               }
        
    }*/
    
    
    @IBAction func agregarItemCarrito(_ sender: Any) {
        
        let itemTitulo = lblTitulo.text ?? ""
        let itemPU = lblPrecio.text ?? ""
        let itemcant = txtCantidad.text ?? ""
        let itemtotal = lblTotal.text ?? ""
        
        /*var item = ItemCarrito(nombre: itemTitulo ?? "",
                               precioUnitario: itemPU ?? "",
                               cantidad: itemcant ?? "",
                               total: itemtotal ?? "")*/
        
        managerDBCarrito.agregar(nombre: itemTitulo, pu: itemPU, cant: itemcant, pt: itemtotal)
        print("Se agrego item al carrito correctamente")
        /*item.nombre = itemTitulo
        item.precioUnitario = itemPU
        item.cantidad = itemcant
        item.total = itemtotal!*/
        
        //print(item.nombre, item.precioUnitario, item.cantidad, item.total)
        
        //listaCarrito.append(item)
    }
    
    @IBAction func calcularTotal(_ sender: Any) {
        
        guard let precio_c = lblPrecio.text, !precio_c.isEmpty else {
            print("El precio es vacio o nulo")
            return
        }
        
        guard let cantidad_c = txtCantidad.text , !cantidad_c.isEmpty else {
            print("la cantidad es vacio o nulo")
            return
        }
        
        let valor1 = Double(precio_c)
        let valor2 = Double(cantidad_c)
        
        let total = valor1! * valor2!
        
        
        //let precioDouble = Double(precio_c)
        //let cant = Double(cantidad_c)
        
        //let total = (Double(precio_c) * Double(cantidad_c))
        
        lblTotal.text = String(total)
        
        print("precio: " + precio_c)
        print("cantidad: " + cantidad_c)
        print("total: " + String(total))
    }
    
    func cargarFoto(){
        let ruta = self.rutaImagen
        let urlImagen = URL(string: ruta)!
        
        if let data = try? Data(contentsOf: urlImagen) {
                   ivImagen?.image = UIImage(data: data)
        }else{
            print("No se pudo cargar la imagen")
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
