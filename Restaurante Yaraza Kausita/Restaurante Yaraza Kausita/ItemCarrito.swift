//
//  ItemCarrito.swift
//  Restaurante Yaraza Kausita
//
//  Created by Henry Daniel on 12/10/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import Foundation

class ItemCarrito{
       let nombre : String
       let precioUnitario :  String
       let cantidad :  String
       let total  :  String
   
       init(nombre : String, precioUnitario : String, cantidad : String, total : String){
           self.nombre = nombre
           self.precioUnitario = precioUnitario
           self.cantidad = cantidad
           self.total = total
       }
}
