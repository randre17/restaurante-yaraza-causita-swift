//
//  ViewController.swift
//  Restaurante Yaraza Kausita
//
//  Created by Rafael on 10/16/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit
import FirebaseAuth

 
class ViewController: UIViewController {
    
 
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtContraseña: UITextField!
    @IBOutlet weak var btnLogin: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func Ingresar(_ sender: Any) {
       
        if let email = txtEmail.text, let password = txtContraseña.text{
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if let result = result, error == nil {

                   
                    self.performSegue(withIdentifier: "goRestaurantes", sender: nil)
                    
                }else {
                    let alertController = UIAlertController(title: "Error", message:
                        "Usuario o contraseña incorrecto", preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction.init(title: "Aceptar", style: .default))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        
        
    }
    
    @IBAction func Registrar(_ sender: Any) {
        performSegue(withIdentifier: "goRegistrar", sender: nil)
    }
}

