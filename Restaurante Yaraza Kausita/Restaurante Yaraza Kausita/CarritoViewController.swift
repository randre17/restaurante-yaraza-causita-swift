//
//  CarritoViewController.swift
//  Restaurante Yaraza Kausita
//
//  Created by Rafael Ramirez on 12/11/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit

class CarritoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tblResumen: UITableView!
    @IBOutlet weak var lblNombre: UILabel!
    
    //var sumatoriaFinal: String = ""
    
    var listaItems = [Carrito]()
    let managerDB = CoreDataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recargarTabla()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnFinalizar(_ sender: Any) {
        print("Selecciono Carrito Resumen")
        performSegue(withIdentifier: "goGenerarPedido", sender: nil)
    }
    
    @IBAction func generarPedido(_ sender: Any) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tableView.dequeueReusableCell(withIdentifier: "celdaCarrito", for: indexPath)
        let lblItem1 = celda.viewWithTag(1) as? UILabel
        let lblItem2 = celda.viewWithTag(2) as? UILabel
        let lblItem3 = celda.viewWithTag(3) as? UILabel
        let lblItem4 = celda.viewWithTag(4) as? UILabel
        
        let objetoCarrito = self.listaItems[indexPath.row]
        
        lblItem1?.text = objetoCarrito.nombrePlatillo
        lblItem2?.text = objetoCarrito.precioUnitario
        lblItem3?.text = objetoCarrito.cantidad
        lblItem4?.text = objetoCarrito.precioTotal

        return celda
    }
    
    func recargarTabla() {
        listaItems = managerDB.obtener()
        print("Se recarga data de la tabla")
        tblResumen.reloadData()

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
